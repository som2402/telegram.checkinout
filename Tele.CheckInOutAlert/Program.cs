﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;

namespace Tele.CheckInOutAlert
{
    class Program
    {
        private const string TOKEN = "1504953264:AAHvwcLNiTiG6uzHAPLBLbnUr3hmv6XMwYQ";
        //private const long GROUP_ALERT_ID = -1001846546048;
        private static ITelegramBotClient botClient = new TelegramBotClient(TOKEN);
        private static IReadOnlyCollection<UserConfig> userConfigs = new List<UserConfig>().AsReadOnly();
        private static IReadOnlyCollection<UserStore> userStores = new List<UserStore>().AsReadOnly();
        private static IgnoreDate ignoreDate = new IgnoreDate()
        {
            Global = new List<string>().AsReadOnly(),
            Dates = new List<DateTime>().AsReadOnly()
        };
        private static IReadOnlyCollection<Telegram.Bot.Types.BotCommand> commands = new List<Telegram.Bot.Types.BotCommand>()
        {
            new Telegram.Bot.Types.BotCommand()
            {
                Command = "checkin_today_info",
                Description = "Thông tin chấm công ngày hôm nay"
            },
            new Telegram.Bot.Types.BotCommand()
            {
                Command = "today_off",
                Description = "Không thông báo chấm công ngày hôm nay"
            },
            new Telegram.Bot.Types.BotCommand()
            {
                Command = "checkin_off",
                Description = "Không thông báo checkin ngày hôm nay"
            },
            new Telegram.Bot.Types.BotCommand()
            {
                Command = "checkout_off",
                Description = "Không thông báo checkout ngày hôm nay"
            },
            new Telegram.Bot.Types.BotCommand()
            {
                Command = "checkin",
                Description = "Đã checkin"
            },
            new Telegram.Bot.Types.BotCommand()
            {
                Command = "checkout",
                Description = "Đã checkout"
            },
            new Telegram.Bot.Types.BotCommand()
            {
                Command = "login_now",
                Description = "Đăng nhập lại hệ thống Misa"
            }
        }.AsReadOnly();
        private static IReadOnlyCollection<TelegramUser> telegramUsers = new List<TelegramUser>().AsReadOnly();
        private static HttpClient httpClient = new HttpClient();

        static void Main(string[] args)
        {
            InitCommand().Wait();
            botClient.OnMessage += BotClient_OnMessage;
            botClient.StartReceiving();

            Log("Start!");
            Log("Running...");

            LoadUserConfigs();
            LoadUserStores();
            LoadIgnoreDate();
            LoadTelegramUsers();

            var tasks = new Func<Task>[]
            {
                AutoReloadUserConfigs,
                AutoReloadIgnoreDate,
                AutoSaveUserStores,
                AutoSaveTelegramUsers,
                CheckAndAlert
            }
            .Select(item => item.Invoke())
            .ToArray();

            Task.WaitAll(tasks);
            Log($"End!");
        }

        private static Task InitCommand()
        {
            return botClient.SetMyCommandsAsync(commands);
        }

        private static void BotClient_OnMessage(object? sender, Telegram.Bot.Args.MessageEventArgs e)
        {
            try
            {
                if (e.Message.Text == null)
                {
                    return;
                }

                var chat = e.Message.Chat;
                var isUserExisting = telegramUsers.Any(item => item.UserId == chat.Id);

                if (isUserExisting)
                {
                    HandleCommand(e.Message.Text, e.Message.Chat).Wait();

                    return;
                }

                var telegramUser = new TelegramUser()
                {
                    UserId = chat.Id,
                    UserName = chat.Username,
                    FirstName = chat.FirstName,
                    LastName = chat.LastName
                };

                telegramUsers = telegramUsers.Append(telegramUser)
                    .ToList()
                    .AsReadOnly();

                SaveTelegramUsers();
                SendWelcomeMessage(telegramUser).Wait();
            }
            catch (Exception ex)
            {
                Log(ex.Message);
            }
        }

        private static async Task HandleCommand(string message, Telegram.Bot.Types.ChatId id)
        {
            if (string.IsNullOrWhiteSpace(message))
            {
                return;
            }
            var stringSplit = message.Split('|');
            string email = string.Empty;
            string password = string.Empty;
            if (stringSplit.Length == 3)
            {
                email = stringSplit[1];
                password = stringSplit[2];
            }
            var commandName = stringSplit[0];
            var isCommand = commands.Any(item => commandName.Equals($"/{item.Command}"));

            if (!isCommand)
            {
                return;
            }

            var userConfig = userConfigs.FirstOrDefault(item => item.UserId == id.Identifier);

            if (userConfig is null)
            {
                await SendMessage(id, "Bạn chưa đăng ký thông tin, vui lòng đăng ký thông tin trước khi sử dụng!");

                return;
            }

            var userStore = userStores.FirstOrDefault(item => item.UserId == id.Identifier);
            var user = new User(userConfig, userStore);
            var command = commands.FirstOrDefault(item => commandName.Equals($"/{item.Command}")).Command;

            switch (command)
            {
                case "checkin_off":
                    {
                        user.Checkin();
                        await SendMessage(id, "Tắt tính năng thông báo checkin thành công!");

                        return;
                    }
                case "checkin":
                    {
                        user.Checkin();
                        await SendMessage(id, "Checkin thành công!");

                        return;
                    }
                case "checkout_off":
                    {
                        user.Checkout();
                        await SendMessage(id, "Tắt tính năng checkout thành công!");

                        return;
                    }
                case "checkout":
                    {
                        user.Checkout();
                        await SendMessage(id, "Checkout thành công!");

                        return;
                    }
                case "today_off":
                    {
                        user.Checkin();
                        user.Checkout();
                        await SendMessage(id, "Tắt tính năng thông báo chekcin, checkout ngày hôm nay thành công!");

                        return;
                    }
                case "checkin_today_info":
                    {
                        //var attendance = await GetAttendanceTracking(userConfig.OMAuthorizeToken);
                        //var attendanceToday = attendance?.data.FirstOrDefault();
                        //if (attendanceToday is null)
                        //{
                        //    await SendMessage(id, "Không thể lấy thông tin chấm công!");

                        //    return;
                        //}
                        //var reply = $@"Thông tin chấm công:{Environment.NewLine}Checkin: <b>{attendanceToday.FirstCheckinText}</b>{Environment.NewLine}Checkout: <b>{attendanceToday.LastCheckoutText}</b>";

                        DateTime today = DateTime.Today;
                        //DateTime dateIn = new DateTime(today.Year, today.Month, today.Day, 08, 00, 00);
                        DateTime dateOut = new DateTime(today.Year, today.Month, today.Day, 17, 30, 00);
                        var anttendanceResponse = await GetAttendanceTracking(userConfig.OMAuthorizeToken, new TimekeepingRemoteRequest()
                        {
                            PageIndex = 1,
                            PageSize = 100,
                            CustomParam = new CustomParams()
                            {
                                EndDate = new DateTime(today.Year, today.Month, today.Day, 22, 00, 00),
                                StartDate = new DateTime(today.Year, today.Month, today.Day, 06, 00, 00),
                            }
                        });
                        if (anttendanceResponse?.Data?.PageData == null || anttendanceResponse?.Data?.PageData.Count() == 0)
                        {
                            await SendMessage(id, "Chưa có thông tin chấm công!");

                            return;
                        }
                        // lấy bản ghi đầu tiên và bản ghi cuối cùng để lấy thời gian in và thời gian out
                        //var attendanceInToday = anttendanceResponse?.Data?.PageData?.
                        //    FirstOrDefault(x => x.CheckTime != null && x.CheckTime.Value.Date == today);

                        var attendanceTodays = anttendanceResponse?.Data?.PageData?.
                          Where(x => x.CheckTime != null && x.CheckTime.Value.Date == today);

                        var attendanceInToday = attendanceTodays.OrderBy(x => x.CreatedDate).FirstOrDefault();

                        var attendanceOutToday = anttendanceResponse?.Data?.PageData?.
                            FirstOrDefault(x => x.CheckTime != null
                            && x.CheckTime >= dateOut && x.CheckTime.Value.Date == today);

                        var reply = $@"Thông tin chấm công:{Environment.NewLine}Checkin: <b>{attendanceInToday?.CheckTime}</b>{Environment.NewLine}Checkout: <b>{attendanceOutToday?.CheckTime}</b>";

                        await SendMessage(id, reply, Telegram.Bot.Types.Enums.ParseMode.Html);

                        return;
                    }
                case "login_now":
                    {
                        if (!string.IsNullOrWhiteSpace(email) && !string.IsNullOrWhiteSpace(password))
                        {
                            await UpdateCookieMisa(email, password, id);
                        }
                        else
                        {
                            var s1 = await botClient.SendTextMessageAsync(id, $"Vui lòng nhập email và mật khẩu của bạn theo cú pháp {Environment.NewLine}/login_now|email|password");
                        }
                        return;
                    }

            }
        }

        private static void Log(string content)
        {
            Console.WriteLine($"{NowDisplay()} {content}");
        }

        private static string NowDisplay() => $"{DateTime.Now:dd-MM-yyyy HH:mm:ss}";
        private static string NowTimeDisplay() => $"{DateTime.Now:HH:mm:ss}";

        private static Task SendMessage(Telegram.Bot.Types.ChatId chatId, string message,
            Telegram.Bot.Types.Enums.ParseMode parseMode = Telegram.Bot.Types.Enums.ParseMode.Default)
        {
            return botClient.SendTextMessageAsync(chatId, message, parseMode);
        }

        private static string ToMessage(string template,
            Telegram.Bot.Types.ChatId chatId,
            string name)
        {
            new List<(string Key, string Value)>()
            {
                ( "Now", NowDisplay() ),
                ( "NowTime", NowTimeDisplay() ),
                ( "UserId", chatId.Identifier.ToString() ),
                ( "UserName", name )
            }
            .ForEach(item => template = template.Replace($"{{{{{item.Key}}}}}", item.Value));

            return template;
        }

        private static Task SendWelcomeMessage(TelegramUser telegramUser)
        {
            var nameParams = new string[]
            {
                telegramUser.FirstName,
                telegramUser.LastName
            }.Where(item => !string.IsNullOrWhiteSpace(item));
            var name = string.Join(" ", nameParams);

            return SendWelcomeMessage(telegramUser.UserId, name);
        }

        private static Task SendWelcomeMessage(Telegram.Bot.Types.ChatId chatId,
            string name = "Bạn",
            string template = "Xin chào {{UserName}}! Rất vui được thông báo cho bạn thông tin chấm công!")
        {
            var message = ToMessage(template, chatId, name);

            return SendMessage(chatId, message, Telegram.Bot.Types.Enums.ParseMode.Html);
        }

        private static Task SendCheckinMessage(Telegram.Bot.Types.ChatId chatId,
            string name = "Bạn",
            string template = "[{{Now}}] Xin thông báo bây giờ là <b><i>{{NowTime}}</i></b> và tôi vẫn chưa thấy {{UserName}} checkin!")
        {
            //var message = $"[{NowDisplay()}] Và 100k nữa lại sắp ra đi í ì i... <a href=\"tg://user?id={userId}\">{name}</a> ơi checkin đi nào ào ào... <b><i>{NowTimeDisplay()}</i></b> rồi ồi ồi...";
            var message = ToMessage(template, chatId, name);

            return SendMessage(chatId, message, Telegram.Bot.Types.Enums.ParseMode.Html);
        }

        private static Task SendCheckoutMessage(Telegram.Bot.Types.ChatId chatId,
            string name = "Bạn",
            string template = "[{{Now}}] Xin thông báo bây giờ là <b><i>{{NowTime}}</i></b> và tôi vẫn chưa thấy {{UserName}} checkout!")
        {
            //var message = $"[{NowDisplay()}] Và 100k nữa lại sắp ra đi í ì i... <a href=\"tg://user?id={userId}\">{name}</a> ơi Checkout đi nào ào ào... <b><i>{NowTimeDisplay()}</i></b> rồi ồi ồi...";
            var message = ToMessage(template, chatId, name);

            return SendMessage(chatId, message, Telegram.Bot.Types.Enums.ParseMode.Html);
        }

        private static bool IsIgnoreDate()
        {

            var isSunday = DateTime.Now.DayOfWeek == DayOfWeek.Sunday;

            if (isSunday)
            {
                return true;
            }
            // nếu là thứ 7 ko phải là thứ 7 đầu tiên trong tháng thì bỏ qua
            DateTime today = DateTime.Today;
            return today.DayOfWeek == DayOfWeek.Saturday && today.Day > 7 ? true : ignoreDate.TodayIsIgnore;
        }

        private static async Task CheckAndAlert()
        {
            while (true)
            {
                try
                {
                    Log("Check start!");

                    if (!IsIgnoreDate())
                    {
                        var users = GetUsers();
                        var checkinUsers = users.Where(item => item.IsNeedToCheckin())
                            .ToList()
                            .AsReadOnly();
                        var checkoutUsers = users.Where(item => item.IsNeedToCheckout())
                            .ToList()
                            .AsReadOnly();

                        AlertCheckin(checkinUsers);
                        AlertCheckout(checkoutUsers);

                        userStores = users.Select(item => item.UserStore)
                            .ToList()
                            .AsReadOnly();
                    }

                    Log($"Check done!{Environment.NewLine}");
                    await Task.Delay(TimeSpan.FromSeconds(1));
                }
                catch (Exception ex)
                {
                    Log(ex.Message);
                }
            }
        }

        private static void AlertCheckin(IReadOnlyCollection<User> checkinUsers)
        {
            if (checkinUsers.Count == 0)
            {
                return;
            }

            Log("Checkin start!");

            var tasks = checkinUsers.Select(async item =>
            {
                (var isCheckin, _) = await IsAttendance(item.UserConfig.OMAuthorizeToken);

                if (isCheckin)
                {
                    item.Checkin();

                    return;
                }

                item.LastCheckin();
                await SendCheckinMessage(item.UserConfig.UserId,
                    item.UserConfig.Name);
            }).ToArray();

            Task.WaitAll(tasks);
            Log("Checkin done!");
        }

        private static void AlertCheckout(IReadOnlyCollection<User> checkoutUsers)
        {
            if (checkoutUsers.Count == 0)
            {
                return;
            }

            Log("Checkout start!");

            var tasks = checkoutUsers.Select(async item =>
            {
                (_, var isCheckout) = await IsAttendance(item.UserConfig.OMAuthorizeToken);

                if (isCheckout)
                {
                    item.Checkout();

                    return;
                }

                item.LastCheckout();
                await SendCheckoutMessage(item.UserConfig.UserId, item.UserConfig.Name);
            }).ToArray();

            Task.WaitAll(tasks);
            Log("Checkout done!");
        }

        private static IReadOnlyCollection<User> GetUsers()
        {
            return userConfigs.Select(ucItem => new User(ucItem, userStores.FirstOrDefault(usItem => usItem.UserId == ucItem.UserId)))
                .ToList()
                .AsReadOnly();
        }

        private static void LoadUserStores()
        {
            userStores = GetUserStores();
        }

        private static void LoadUserConfigs()
        {
            userConfigs = GetUserConfigs();
        }

        private static void LoadIgnoreDate()
        {
            ignoreDate = GetIgnoreDate();
        }

        private static void LoadTelegramUsers()
        {
            telegramUsers = GetTelegramUsers();
        }

        private static async Task AutoReloadUserConfigs()
        {
            while (true)
            {
                LoadUserConfigs();
                await Task.Delay(TimeSpan.FromSeconds(5));
            }
        }

        private static async Task AutoSaveUserStores()
        {
            while (true)
            {
                SaveUserStore();
                await Task.Delay(TimeSpan.FromSeconds(1));
            }
        }

        private static async Task AutoSaveTelegramUsers()
        {
            while (true)
            {
                SaveTelegramUsers();
                await Task.Delay(TimeSpan.FromSeconds(1));
            }
        }

        private static async Task AutoReloadIgnoreDate()
        {
            while (true)
            {
                LoadIgnoreDate();
                await Task.Delay(TimeSpan.FromSeconds(5));
            }
        }

        private static IReadOnlyCollection<UserConfig> GetUserConfigs()
        {
            try
            {
                var content = File.ReadAllText("./user-config.json");

                return JsonConvert.DeserializeObject<IReadOnlyCollection<UserConfig>>(content) ?? new List<UserConfig>().AsReadOnly();
            }
            catch
            {
                return new List<UserConfig>().AsReadOnly();
            }
        }

        private static IReadOnlyCollection<UserStore> GetUserStores()
        {
            try
            {
                var content = File.ReadAllText("./user-store.json");

                return JsonConvert.DeserializeObject<IReadOnlyCollection<UserStore>>(content) ?? new List<UserStore>().AsReadOnly();
            }
            catch
            {
                return new List<UserStore>().AsReadOnly();
            }
        }

        private static IgnoreDate GetIgnoreDate()
        {
            try
            {
                var content = File.ReadAllText("./ignore-date.json");

                return JsonConvert.DeserializeObject<IgnoreDate>(content) ?? new IgnoreDate()
                {
                    Global = new List<string>().AsReadOnly(),
                    Dates = new List<DateTime>().AsReadOnly()
                };
            }
            catch
            {
                return new IgnoreDate()
                {
                    Global = new List<string>().AsReadOnly(),
                    Dates = new List<DateTime>().AsReadOnly()
                };
            }
        }

        private static IReadOnlyCollection<TelegramUser> GetTelegramUsers()
        {
            try
            {
                var content = File.ReadAllText("./telegram-user.json");

                return JsonConvert.DeserializeObject<IReadOnlyCollection<TelegramUser>>(content) ?? new List<TelegramUser>().AsReadOnly();
            }
            catch
            {
                return new List<TelegramUser>().AsReadOnly();
            }
        }

        private static void SaveUserStore()
        {
            try
            {
                var content = JsonConvert.SerializeObject(userStores);

                File.WriteAllText("./user-store.json", content);
            }
            catch
            {
            }
        }

        private static void SaveTelegramUsers()
        {
            try
            {
                var content = JsonConvert.SerializeObject(telegramUsers);

                File.WriteAllText("./telegram-user.json", content);
            }
            catch
            {
            }
        }

        private static async Task<(bool IsCheckin, bool IsCheckout)> IsAttendance(string authorizeToken)
        {
            //var anttendanceResponse = await GetAttendanceTracking(authorizeToken);
            //if (anttendanceResponse is null)
            //{
            //    return (false, false);
            //}

            //if (anttendanceResponse.data == null || anttendanceResponse.data.Length == 0)
            //{
            //    return (false, false);
            //}

            //var lastAttendance = anttendanceResponse.data.FirstOrDefault();
            //var isCheckin = !string.IsNullOrWhiteSpace(lastAttendance.FirstCheckin);
            //var isCheckout = !string.IsNullOrWhiteSpace(lastAttendance.LastCheckout);

            //return (isCheckin, isCheckout);

            DateTime today = DateTime.Today;
            var anttendanceResponse = await GetAttendanceTracking(authorizeToken, new TimekeepingRemoteRequest()
            {
                PageIndex = 1,
                PageSize = 100,
                CustomParam = new CustomParams()
                {
                    EndDate = new DateTime(today.Year, today.Month, today.Day, 22, 00, 00),
                    StartDate = new DateTime(today.Year, today.Month, today.Day, 06, 00, 00),
                }
            });

            if (anttendanceResponse?.Data?.PageData == null || anttendanceResponse?.Data?.PageData.Count() == 0)
            {
                return (false, false);
            }
            DateTime dateNow = DateTime.Now;
            DateTime dateIn = new DateTime(today.Year, today.Month, today.Day, 08, 00, 00);
            DateTime dateOut = new DateTime(today.Year, today.Month, today.Day, 17, 30, 00);

            // lấy bản ghi đầu tiên và bản ghi cuối cùng để lấy thời gian in và thời gian out
            //var attendanceInToday = anttendanceResponse?.Data?.PageData?.
            //    FirstOrDefault(x => x.CheckTime != null && x.CheckTime.Value.Date == today);

            var attendanceTodays = anttendanceResponse?.Data?.PageData?.
              Where(x => x.CheckTime != null && x.CheckTime.Value.Date == today);

            var attendanceInToday = attendanceTodays.OrderBy(x => x.CreatedDate).FirstOrDefault();

            var attendanceOutToday = anttendanceResponse?.Data?.PageData?.
                FirstOrDefault(x => x.CheckTime != null
                && x.CheckTime >= dateOut && x.CheckTime.Value.Date == today);

            var reply = $@"Thông tin chấm công:{Environment.NewLine}Checkin: <b>{attendanceInToday?.CheckTime}</b>{Environment.NewLine}Checkout: <b>{attendanceOutToday?.CheckTime}</b>";


            var isCheckin = attendanceInToday?.CheckTime != null;
            var isCheckout = attendanceOutToday?.CheckTime != null;

            return (isCheckin, isCheckout);
        }

        private static async Task<AttendanceTrackingResponse> GetAttendanceTracking2(string authorizeToken)
        {
            if (string.IsNullOrWhiteSpace(authorizeToken))
            {
                throw new ArgumentNullException(nameof(authorizeToken));
            }

            const string REQUEST_CONTENT = "{\"draw\":1,\"columns\":[{\"data\":\"WorkingDate\",\"name\":\"\",\"searchable\":true,\"orderable\":false,\"search\":{\"value\":\"\",\"regex\":false}},{\"data\":\"FirstCheckin\",\"name\":\"\",\"searchable\":true,\"orderable\":false,\"search\":{\"value\":\"\",\"regex\":false}},{\"data\":\"LastCheckout\",\"name\":\"\",\"searchable\":true,\"orderable\":false,\"search\":{\"value\":\"\",\"regex\":false}},{\"data\":\"TotalWorkingHoursText\",\"name\":\"\",\"searchable\":true,\"orderable\":false,\"search\":{\"value\":\"\",\"regex\":false}}],\"order\":[],\"start\":0,\"length\":1,\"search\":{\"value\":\"\",\"regex\":false}}";
            var requestContent = new StringContent(REQUEST_CONTENT, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(HttpMethod.Post, "http://om-api.ichibajp.com/api/Dashboard/GetAttendanceTracking")
            {
                Content = requestContent
            };

            request.Headers.TryAddWithoutValidation("Authorization", authorizeToken);

            var response = await httpClient.SendAsync(request);

            response.EnsureSuccessStatusCode();

            var responseContent = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<AttendanceTrackingResponse>(responseContent);
        }

        private static async Task<TimekeepingRemoteResponse> GetAttendanceTracking(string cookie, TimekeepingRemoteRequest timekeepingRemoteRequest)
        {
            if (string.IsNullOrWhiteSpace(cookie))
            {
                throw new ArgumentNullException(nameof(cookie));
            }

            var REQUEST_CONTENT = JsonConvert.SerializeObject(timekeepingRemoteRequest);
            var requestContent = new StringContent(REQUEST_CONTENT, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(HttpMethod.Post, "https://amisapp.misa.vn/APIS/EmployeeCnBAPI/api/TimekeepingRemote/paging")
            {
                Content = requestContent
            };

            request.Headers.TryAddWithoutValidation("Cookie", cookie);

            var response = await httpClient.SendAsync(request);

            response.EnsureSuccessStatusCode();

            var responseContent = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<TimekeepingRemoteResponse>(responseContent);
        }

        private static async Task UpdateCookieMisa(string userName, string password, Telegram.Bot.Types.ChatId id)
        {
            if (string.IsNullOrWhiteSpace(userName) || string.IsNullOrWhiteSpace(password))
            {
                throw new ArgumentNullException(nameof(userName));
            }
            var requestLogin = new
            {
                UserName = userName,
                Password = password
            };
            var REQUEST_CONTENT = JsonConvert.SerializeObject(requestLogin);
            var requestContent = new StringContent(REQUEST_CONTENT, Encoding.UTF8, "application/json");
            var request = new HttpRequestMessage(HttpMethod.Post, "https://amisapp.misa.vn/APIS/AuthenAPI/api/Account/login")
            {
                Content = requestContent
            };

            var response = await httpClient.SendAsync(request);

            response.EnsureSuccessStatusCode();
            var responseContent = await response.Content.ReadAsStringAsync();
            var misaResponseLogin = JsonConvert.DeserializeObject<MisaLoginResponse>(responseContent);
            if (misaResponseLogin == null || !misaResponseLogin.Success)
            {
                await SendMessage(id, !string.IsNullOrWhiteSpace(misaResponseLogin?.UserMessage) ? misaResponseLogin?.UserMessage : "Đăng nhập thất bại, vui lòng check lại api đăng nhập của misa");
                return;
            }

            var listCookie = new List<string>();
            if (response.Headers.TryGetValues("Set-Cookie", out var cookieValues))
            {
                foreach (var cookieValue in cookieValues)
                {
                    // Process each cookie value
                    listCookie.Add(cookieValue);
                }
            }

            var strCookie = string.Join(";", listCookie);

            string jsonFilePath = "user-config.json";
            string json = File.ReadAllText(jsonFilePath);
            List<UserConfig> userList = JsonConvert.DeserializeObject<List<UserConfig>>(json);
            var userToUpdate = userList.Find(u => u.UserId == id.Identifier);
            if (userToUpdate != null)
            {
                userToUpdate.OMAuthorizeToken = strCookie;
                string updatedJson = JsonConvert.SerializeObject(userList, Formatting.Indented);
                File.WriteAllText(jsonFilePath, updatedJson);
                await SendMessage(id, "Misa Authorize Cookie thành công!");
            }
            else
            {
                await SendMessage(id, "Misa Authorize Cookie thất bại , không tìm thấy thông tin người dùng!");
            }
            return;
        }
    }
}
