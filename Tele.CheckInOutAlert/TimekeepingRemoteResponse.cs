﻿using Newtonsoft.Json;
using System;

namespace Tele.CheckInOutAlert
{
    public partial class TimekeepingRemoteResponse
    {
        [JsonProperty("ValidateInfo")]
        public object[] ValidateInfo { get; set; }

        [JsonProperty("Success")]
        public bool Success { get; set; }

        [JsonProperty("Code")]
        public long Code { get; set; }

        [JsonProperty("SubCode")]
        public long SubCode { get; set; }

        [JsonProperty("UserMessage")]
        public object UserMessage { get; set; }

        [JsonProperty("SystemMessage")]
        public object SystemMessage { get; set; }

        [JsonProperty("Data")]
        public Data Data { get; set; }

        [JsonProperty("GetLastData")]
        public bool GetLastData { get; set; }

        [JsonProperty("ServerTime")]
        public DateTime? ServerTime { get; set; }
    }

    public partial class Data
    {
        [JsonProperty("SummaryData")]
        public object SummaryData { get; set; }

        [JsonProperty("PageData")]
        public PageDatum[] PageData { get; set; }

        [JsonProperty("Total")]
        public long Total { get; set; }

        [JsonProperty("CustomData")]
        public CustomData CustomData { get; set; }
    }

    public partial class CustomData
    {
        [JsonProperty("WorkingShift")]
        public WorkingShift WorkingShift { get; set; }
    }

    public partial class WorkingShift
    {
        [JsonProperty("IsWorkingShift")]
        public bool IsWorkingShift { get; set; }

        [JsonProperty("StartWorkingShift")]
        public DateTime? StartWorkingShift { get; set; }

        [JsonProperty("EndWorkingShift")]
        public DateTime? EndWorkingShift { get; set; }

        [JsonProperty("StartNextWorkingShift")]
        public object StartNextWorkingShift { get; set; }

        [JsonProperty("EndNextWorkingShift")]
        public object EndNextWorkingShift { get; set; }

        [JsonProperty("IsChecking")]
        public bool IsChecking { get; set; }

        [JsonProperty("IsTimekeepingRemote")]
        public bool IsTimekeepingRemote { get; set; }

        [JsonProperty("IsRequireAttachment")]
        public bool IsRequireAttachment { get; set; }

        [JsonProperty("IsCheckWorkingShift")]
        public bool IsCheckWorkingShift { get; set; }

        [JsonProperty("IsBreakWorkingShift")]
        public bool IsBreakWorkingShift { get; set; }

        [JsonProperty("CountBreakChecking")]
        public long CountBreakChecking { get; set; }

        [JsonProperty("IsManagerConfirmTimekeeping")]
        public bool IsManagerConfirmTimekeeping { get; set; }

        [JsonProperty("IsRequireFaceIdentifi")]
        public bool IsRequireFaceIdentifi { get; set; }

        [JsonProperty("WorkingShiftID")]
        public long WorkingShiftId { get; set; }

        [JsonProperty("WorkingShiftCode")]
        public string WorkingShiftCode { get; set; }

        [JsonProperty("WorkingShiftName")]
        public string WorkingShiftName { get; set; }

        [JsonProperty("ApprovalToID")]
        public long ApprovalToId { get; set; }

        [JsonProperty("ApprovalName")]
        public string ApprovalName { get; set; }

        [JsonProperty("ApprovalJobPosition")]
        public string ApprovalJobPosition { get; set; }

        [JsonProperty("ApprovalOrganizationUnit")]
        public string ApprovalOrganizationUnit { get; set; }

        [JsonProperty("StartTime")]
        public DateTime? StartTime { get; set; }

        [JsonProperty("EndTime")]
        public DateTime? EndTime { get; set; }

        [JsonProperty("IsCheckOutWorkingShift")]
        public bool IsCheckOutWorkingShift { get; set; }

        [JsonProperty("IsSettingCheckOutWorkingShift")]
        public bool IsSettingCheckOutWorkingShift { get; set; }

        [JsonProperty("IsSettingCheckInWorkingShift")]
        public bool IsSettingCheckInWorkingShift { get; set; }

        [JsonProperty("IsRequireWifi")]
        public bool IsRequireWifi { get; set; }

        [JsonProperty("WifiIDs")]
        public string WifiIDs { get; set; }

        [JsonProperty("WifiSettings")]
        public WifiSetting[] WifiSettings { get; set; }

        [JsonProperty("IsRequireGPS")]
        public bool IsRequireGps { get; set; }

        [JsonProperty("BlockFakeGPS")]
        public bool BlockFakeGps { get; set; }

        [JsonProperty("GPSIDs")]
        public object GpsiDs { get; set; }

        [JsonProperty("IsGPSFixed")]
        public bool IsGpsFixed { get; set; }

        [JsonProperty("GPSSettings")]
        public object[] GpsSettings { get; set; }

        [JsonProperty("TimeKeepingRemote")]
        public PageDatum TimeKeepingRemote { get; set; }

        [JsonProperty("QRCodeIDs")]
        public object QrCodeIDs { get; set; }

        [JsonProperty("IsRequireQrCode")]
        public bool IsRequireQrCode { get; set; }

        [JsonProperty("QRCodeSettings")]
        public object[] QrCodeSettings { get; set; }

        [JsonProperty("ReplaceTimekeeping")]
        public bool ReplaceTimekeeping { get; set; }

        [JsonProperty("ReplaceTimekeepingData")]
        public ReplaceTimekeepingData ReplaceTimekeepingData { get; set; }
    }

    public partial class ReplaceTimekeepingData
    {
    }

    public partial class PageDatum
    {
        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("TimekeepingRemoteID")]
        public long TimekeepingRemoteId { get; set; }

        [JsonProperty("EmployeeID")]
        public long EmployeeId { get; set; }

        [JsonProperty("FullName")]
        public string FullName { get; set; }

        [JsonProperty("EmployeeName")]
        public string EmployeeName { get; set; }

        [JsonProperty("EmployeeCode")]
        public string EmployeeCode { get; set; }

        [JsonProperty("OrganizationUnitID")]
        public long OrganizationUnitId { get; set; }

        [JsonProperty("OrganizationUnitName")]
        public string OrganizationUnitName { get; set; }

        [JsonProperty("Documents")]
        public string Documents { get; set; }

        [JsonProperty("ListDocuments")]
        public object[] ListDocuments { get; set; }

        [JsonProperty("CheckTime")]
        public DateTime? CheckTime { get; set; }

        [JsonProperty("WorkingShiftID")]
        public long WorkingShiftId { get; set; }

        [JsonProperty("WorkingShiftCode")]
        public string WorkingShiftCode { get; set; }

        [JsonProperty("WorkingShiftName")]
        public string WorkingShiftName { get; set; }

        [JsonProperty("TimeKeeperDataCode")]
        public long TimeKeeperDataCode { get; set; }

        [JsonProperty("JobPositionID")]
        public long JobPositionId { get; set; }

        [JsonProperty("JobPositionName")]
        public string JobPositionName { get; set; }

        [JsonProperty("Time", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? Time { get; set; }

        [JsonProperty("ApprovalToID")]
        public long ApprovalToId { get; set; }

        [JsonProperty("ApprovalName")]
        public string ApprovalName { get; set; }

        [JsonProperty("Status")]
        public long Status { get; set; }

        [JsonProperty("StartWorkingShift")]
        public DateTime? StartWorkingShift { get; set; }

        [JsonProperty("EndWorkingShift")]
        public DateTime? EndWorkingShift { get; set; }

        [JsonProperty("WifiName")]
        public string WifiName { get; set; }

        [JsonProperty("GPSName")]
        public object GpsName { get; set; }

        [JsonProperty("Latitude")]
        public long Latitude { get; set; }

        [JsonProperty("Longitude")]
        public long Longitude { get; set; }

        [JsonProperty("QRCodeName")]
        public object QrCodeName { get; set; }

        [JsonProperty("WorkLocationID")]
        public long WorkLocationId { get; set; }

        [JsonProperty("WorkLocationName")]
        public string WorkLocationName { get; set; }

        [JsonProperty("WorkLocationCode")]
        public string WorkLocationCode { get; set; }

        [JsonProperty("TenantID")]
        public Guid TenantId { get; set; }

        [JsonProperty("CreatedDate")]
        public DateTime? CreatedDate { get; set; }

        [JsonProperty("CreatedBy")]
        public string CreatedBy { get; set; }

        [JsonProperty("ModifiedDate")]
        public DateTime? ModifiedDate { get; set; }

        [JsonProperty("ModifiedBy")]
        public string ModifiedBy { get; set; }

        [JsonProperty("EditVersion", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime? EditVersion { get; set; }

        [JsonProperty("State", NullValueHandling = NullValueHandling.Ignore)]
        public long? State { get; set; }

        [JsonProperty("OldData")]
        public object OldData { get; set; }

        [JsonProperty("PassWarningCode")]
        public object PassWarningCode { get; set; }

        [JsonProperty("DataSourceID", NullValueHandling = NullValueHandling.Ignore)]
        public long? DataSourceId { get; set; }

        [JsonProperty("IsGPSFixed", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsGpsFixed { get; set; }
    }

    public partial class WifiSetting
    {
        [JsonProperty("WifiID")]
        public long WifiId { get; set; }

        [JsonProperty("WifiName")]
        public string WifiName { get; set; }

        [JsonProperty("BSSID")]
        public string Bssid { get; set; }

        [JsonProperty("WorkLocationID")]
        public long WorkLocationId { get; set; }

        [JsonProperty("WorkLocationName")]
        public string WorkLocationName { get; set; }

        [JsonProperty("WorkLocationCode")]
        public string WorkLocationCode { get; set; }

        [JsonProperty("TenantID")]
        public Guid TenantId { get; set; }

        [JsonProperty("CreatedDate")]
        public DateTime? CreatedDate { get; set; }

        [JsonProperty("CreatedBy")]
        public string CreatedBy { get; set; }

        [JsonProperty("ModifiedDate")]
        public DateTime? ModifiedDate { get; set; }

        [JsonProperty("ModifiedBy")]
        public string ModifiedBy { get; set; }

        [JsonProperty("EditVersion")]
        public DateTime? EditVersion { get; set; }

        [JsonProperty("State")]
        public long State { get; set; }

        [JsonProperty("OldData")]
        public object OldData { get; set; }

        [JsonProperty("PassWarningCode")]
        public object PassWarningCode { get; set; }
    }

    public partial class MisaLoginResponse
    {
        //[JsonProperty("ValidateInfo")]
        //public object[] ValidateInfo { get; set; }

        [JsonProperty("Success")]
        public bool Success { get; set; }

        [JsonProperty("Code")]
        public long Code { get; set; }

        [JsonProperty("SubCode")]
        public long SubCode { get; set; }

        [JsonProperty("UserMessage")]
        public string UserMessage { get; set; }

        [JsonProperty("SystemMessage")]
        public string SystemMessage { get; set; }

        //[JsonProperty("Data")]
        //public object Data { get; set; }

        [JsonProperty("GetLastData")]
        public bool GetLastData { get; set; }

        //[JsonProperty("ServerTime")]
        //public DateTimeOffset ServerTime { get; set; }
    }
}
