﻿using System;

namespace Tele.CheckInOutAlert
{
    public class TimekeepingRemoteRequest
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string Filter { get; set; }
        public string Sort { get; set; }
        public CustomParams CustomParam { get; set; }
    }

    public class CustomParams
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
