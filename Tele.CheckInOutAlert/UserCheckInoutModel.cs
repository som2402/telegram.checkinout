﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Tele.CheckInOutAlert
{
    public class UserConfig
    {
        public long UserId { get; set; }
        public string Name { get; set; }
        public string OMAuthorizeToken { get; set; }

        public TimeSpan CheckinFrom { get; set; }
        public TimeSpan CheckinTo { get; set; }
        public int CheckinAlertCount { get; set; }
        public int CheckinAlertDelaySecond { get; set; }

        public TimeSpan CheckoutFrom { get; set; }
        public TimeSpan CheckoutTo { get; set; }
        public int CheckoutAlertCount { get; set; }
        public int CheckoutAlertDelaySecond { get; set; }
    }

    public class UserStore
    {
        public long UserId { get; set; }

        public DateTime LastCheckinTime { get; set; }
        public bool Checkin { get; set; }
        public int CheckinAlertCount { get; set; }

        public DateTime LastCheckoutTime { get; set; }
        public bool Checkout { get; set; }
        public int CheckoutAlertCount { get; set; }
    }

    public class IgnoreDate
    {
        public IReadOnlyCollection<string> Global { get; set; }
        public IReadOnlyCollection<DateTime> Dates { get; set; }

        public IReadOnlyCollection<DateTime> AllDates
        {
            get
            {
                var globalDates = Global.Select(item => $"{DateTime.Now.Year}-{item}")
                    .Select(item => DateTime.Parse(item));

                return Dates.Concat(globalDates).ToList()
                    .AsReadOnly();
            }
        }

        public bool TodayIsIgnore => AllDates.Any(item => item == DateTime.Parse(DateTime.Now.ToShortDateString()));
    }

    public class User
    {
        private DateTime now => DateTime.Now;
        private DateTime today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
        public UserConfig UserConfig { get; }
        public UserStore UserStore { get; }

        public DateTime CheckinFrom => today.Add(UserConfig.CheckinFrom);
        public DateTime CheckinTo => today.Add(UserConfig.CheckinTo);

        public DateTime CheckoutFrom => today.Add(UserConfig.CheckoutFrom);
        public DateTime CheckoutTo => today.Add(UserConfig.CheckoutTo);

        public User(UserConfig userConfig, UserStore? userStore)
        {
            if (userStore is null)
            {
                userStore = new UserStore()
                {
                    UserId = userConfig.UserId,
                    LastCheckinTime = DateTime.MinValue,
                    LastCheckoutTime = DateTime.MinValue
                };
            }
            else
            {
                var lastCheckinDate = DateTime.Parse(userStore.LastCheckinTime.ToShortDateString());
                var lastCheckoutDate = DateTime.Parse(userStore.LastCheckoutTime.ToShortDateString());

                if (lastCheckinDate < today)
                {
                    userStore.Checkin = false;
                    userStore.CheckinAlertCount = 0;
                }

                if (lastCheckoutDate < today)
                {
                    userStore.Checkout = false;
                    userStore.CheckoutAlertCount = 0;
                }
            }

            UserConfig = userConfig;
            UserStore = userStore;
        }

        public bool IsNeedToCheckin()
        {
            if (UserStore.Checkin)
            {
                return false;
            }

            var isInCheckinTime = CheckinFrom <= now
                && now <= CheckinTo;

            if (!isInCheckinTime)
            {
                return false;
            }

            if (UserStore.CheckinAlertCount >= UserConfig.CheckinAlertCount)
            {
                return false;
            }

            if (UserStore.LastCheckinTime.AddSeconds(UserConfig.CheckinAlertDelaySecond) > now)
            {
                return false;
            }

            return true;
        }

        public bool IsNeedToCheckout()
        {
            if (UserStore.Checkout)
            {
                return false;
            }

            var isoutCheckoutTime = CheckoutFrom <= now
                && now <= CheckoutTo;

            if (!isoutCheckoutTime)
            {
                return false;
            }

            if (UserStore.CheckoutAlertCount >= UserConfig.CheckoutAlertCount)
            {
                return false;
            }

            if (UserStore.LastCheckoutTime.AddSeconds(UserConfig.CheckoutAlertDelaySecond) > now)
            {
                return false;
            }

            return true;
        }

        public void Checkin()
        {
            UserStore.Checkin = true;
            UserStore.CheckinAlertCount = 0;
            UserStore.LastCheckinTime = DateTime.Now;
        }

        public void LastCheckin()
        {
            UserStore.LastCheckinTime = DateTime.Now;
            UserStore.CheckinAlertCount++;
        }

        public void Checkout()
        {
            UserStore.Checkout = true;
            UserStore.CheckoutAlertCount = 0;
            UserStore.LastCheckoutTime = DateTime.Now;
        }

        public void LastCheckout()
        {
            UserStore.LastCheckoutTime = DateTime.Now;
            UserStore.CheckoutAlertCount++;
        }
    }

    public class TelegramUser
    {
        public long UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class AttendanceTrackingResponse
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public AttendanceTracking[] data { get; set; }
    }

    public class AttendanceTracking
    {
        public string Id { get; set; }
        public string EmployeeId { get; set; }
        public string WorkingDate { get; set; }
        public string WorkingDateText { get; set; }
        public string FirstCheckin { get; set; }
        public string FirstCheckinText { get; set; }
        public string CheckoutInBetween { get; set; }
        public string CheckoutInBetweenText { get; set; }
        public string CheckinInBetween { get; set; }
        public string CheckinInBetweenText { get; set; }
        public string LastCheckout { get; set; }
        public string LastCheckoutText { get; set; }
        public string TotalWorkingHours { get; set; }
        public string TotalWorkingHoursText { get; set; }
        public string Total_LateInPenAmount { get; set; }
        public string Total_LateInPenAmountText { get; set; }
        public string Total_EarlyOutPenAmount { get; set; }
        public string Total_EarlyOutPenAmountText { get; set; }
        public string Total_ForgotAttPenAmount { get; set; }
        public string Total_ForgotAttPenAmountText { get; set; }
        public int _STT { get; set; }
    }
}
